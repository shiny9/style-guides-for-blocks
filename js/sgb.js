(function($) {
	var sgbAddNonce = sgbAjax.sgbAddNonce;
	var sgbDeleteNonce = sgbAjax.sgbDeleteNonce; // Upcoming

	$( '#sgb-form' ).submit( function( e ) {
		var selectedTypes = [];
		jQuery( '.sgb-cpt-type' ).each( function() {
			if( this.checked ) {
				selectedTypes.push( this.dataset.cpt );
			}
		} );

		$.ajax({
			url: ajaxurl,
			type: 'post',
			data: {
				action: 'sgb_add_posts',
				sgbAddNonce: sgbAddNonce,
				selectedTypes: selectedTypes
			},

			success: function( data ) {
				console.log( 'howdy' );
			},

			error: function( XMLHttpRequest, testStatus, errorThrown ) {
				console.log( XMLHttpRequest );
				console.log( testStatus );
				console.log( errorThrown );
			}
		} );
		return true;
	} );
})( jQuery );