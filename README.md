## Plugin Name
Contributors: robertgillmer<br/>
Donate link: https://robertgillmer.com/<br/>
Tags: gutenberg, gutenberg blocks, gutenberg styling, blocks, block styling<br/>
Requires at least: 5.0<br/>
Tested up to: 5.1.0<br/>
Stable tag: 1.0.0<br/>
Requires PHP: 5.3<br/>
License: GPLv2 or later<br/>
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Create "style guides" displaying all core WordPress Gutenberg blocks, including variations, for styling purposes.

### Description

This plugin allows you to create "style guides" displaying all core WordPress Gutenberg blocks, including variations, for styling purposes.  You can choose which custom post types you can create the posts for, so you can see if there are CSS differences across different CPT's.  Additionally, you can use this to test how Gutenberg blocks display on custom post types which haven't been set up to use the Gutenberg editor yet.

### Installation


1. Install the plugin through the Add Plugins screen.
1. Or, if you're feeling adventurous, download the plugin, unzip it, and upload it to the /wp-content/plugins folder via (S)FTP.
1. But really, do that first one, all the cool kids are doing it. 
1. Once it's activated, go to Tools / Gutenberg Blog Style Guides to create the posts.


### Frequently Asked Questions

**Can this plugin be used to test custom post types which don't support Gutenberg Editor yet?**

Yes it can.  The Gutenberg mockup that this plugin puts into `the_content` will be parsed by WordPress just as if it had been entered through the editor directly.

**Can I choose which blocks to display?**

That will be offered in a future release.

**Do you only create pages for core WordPress Gutenberg blocks?**

That's also on the roadmap.  There will be pages for blocks from:

* [Atomic Blocks](https://wordpress.org/plugins/atomic-blocks/)
* [Block Gallery](https://wordpress.org/plugins/block-gallery/)
* [CoBlocks](https://wordpress.org/plugins/coblocks/)
* [Ultimate Addons for Gutenberg](https://wordpress.org/plugins/ultimate-addons-for-gutenberg/)

### Screenshots

![The main screen for Gutenberg Block Style Guides.] (./assets/screenshot-1.png)

1. The main screen for Gutenberg Block Style Guides.

### Changelog

**1.0**

Initial commit