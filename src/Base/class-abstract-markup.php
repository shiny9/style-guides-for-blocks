<?php

namespace SGB\Base;

abstract class Abstract_Markup {
	public $very_short_lorem;
	public $short_lorem;
	public $medium_lorem;
	public $long_lorem;
	public $custom_color;
	public $markup;

	public function __construct() {
		$this->custom_color = $this->get_custom_color();
		$this->markup = $this->get_markup();
	}

	protected function get_lorem( $length = '' ) {
		$very_short = apply_filters( 'sgb-very-short-lorem', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo quisque a mattis nisi dapibus a suspendisse.' );
		$short = apply_filters( 'sgb-short-lorem', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo quisque a mattis nisi dapibus a suspendisse ac ullamcorper conubia ut non eget potenti orci laoreet et condimentum a nibh dis.' );
		$medium = apply_filters( 'sgb-medium-lorem', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo quisque a mattis nisi dapibus a suspendisse ac ullamcorper conubia ut non eget potenti orci laoreet et condimentum a nibh dis. Magnis nullam consectetur per scelerisque quam feugiat a convallis iaculis morbi torquent nulla viverra consectetur elementum purus vestibulum vitae nam.' );
		$long = apply_filters( 'sgb-long-lorem','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo quisque a mattis nisi dapibus a suspendisse ac ullamcorper conubia ut non eget potenti orci laoreet et condimentum a nibh dis. Magnis nullam consectetur per scelerisque quam feugiat a convallis iaculis morbi torquent nulla viverra consectetur elementum purus vestibulum vitae nam. Mi scelerisque vestibulum adipiscing vestibulum nam gravida neque volutpat facilisi eros diam bibendum mi cursus cras scelerisque luctus bibendum himenaeos enim pulvinar felis euismod.' );
		switch( $length ) {
			case 'very-short':
				return $very_short;
				break;
			case 'short':
				return $short;
				break;
			case 'medium':
				return $medium;
				break;
			case 'long':
				return $long;
				break;
			default:
				return;
				break;
		}
	}

	protected function get_custom_color() {
		return apply_filters( 'sgb-custom-color', '#bada55' );
	}

	// protected abstract function get_list_of_blocks();
	protected abstract function get_markup();
}