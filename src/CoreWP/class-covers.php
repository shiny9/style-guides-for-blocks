<?php

namespace SGB\CoreWP;

use SGB\Base\Abstract_Markup;

class Covers extends Abstract_Markup {

	public function __construct() {
		$this->medium_lorem = $this->get_lorem( 'medium' );
		parent::__construct();
	}

	protected function get_markup() {
		ob_start(); ?>
		<!-- wp:html -->
		<h4 style="clear:both;text-align:center;">Cover Image</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900"} -->
		<div class="wp-block-cover has-background-dim" style="background-image:url(https://via.placeholder.com/1600x900)"></div>
		<!-- /wp:cover -->

		<!-- wp:html -->
		<h4 style="clear:both;text-align:center;">Cover Image with Title</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900"} -->
		<div class="wp-block-cover has-background-dim" style="background-image:url(https://via.placeholder.com/1600x900)"><p class="wp-block-cover-text">Cover Image with a Title</p></div>
		<!-- /wp:cover -->

		<!-- wp:html -->
		<h4 style="clear: both;text-align:center;">Cover Image with an Overlay</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900","customOverlayColor":"<?php echo $this->custom_color; ?>"} -->
		<div class="wp-block-cover has-background-dim" style="background-image:url(https://via.placeholder.com/1600x900);background-color:<?php echo $this->custom_color; ?>"></div>
		<!-- /wp:cover -->

		<!-- wp:html -->
		<h4 style="clear:both;text-align:center;">Left-aligned Cover Image</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900","align":"left"} -->
		<div class="wp-block-cover has-background-dim alignleft" style="background-image:url(https://via.placeholder.com/1600x900)"></div>
		<!-- /wp:cover -->

		<!-- wp:paragraph -->
		<p><?php echo $this->medium_lorem; ?></p>
		<!-- /wp:paragraph -->

		<!-- wp:html -->
		<h4 style="clear:both;text-align:center;">Centered Cover Image</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900","align":"center"} -->
		<div class="wp-block-cover has-background-dim aligncenter" style="background-image:url(https://via.placeholder.com/1600x900)"></div>
		<!-- /wp:cover -->

		<!-- wp:html -->
		<h4 style="clear:both;text-align:center;">Right-aligned Cover Image</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900","align":"right"} -->
		<div class="wp-block-cover has-background-dim alignright" style="background-image:url(https://via.placeholder.com/1600x900)"></div>
		<!-- /wp:cover -->

		<!-- wp:paragraph -->
		<p><?php echo $this->medium_lorem; ?></p>
		<!-- /wp:paragraph -->

		<!-- wp:html -->
		<h4 style="clear:both;text-align:center;">Wide-aligned Cover Image</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900","align":"wide"} -->
		<div class="wp-block-cover has-background-dim alignwide" style="background-image:url(https://via.placeholder.com/1600x900)"></div>
		<!-- /wp:cover -->

		<!-- wp:html -->
		<h4 style="clear:both;text-align:center;">Full Width Cover Image</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900","align":"full"} -->
		<div class="wp-block-cover has-background-dim alignfull" style="background-image:url(https://via.placeholder.com/1600x900)"></div>
		<!-- /wp:cover -->

		<!-- wp:html -->
		<h4 style="clear:both;text-align:center;">Full Width Fixed Cover Image</h4>
		<!-- /wp:html -->

		<!-- wp:cover {"url":"https://via.placeholder.com/1600x900","align":"full","hasParallax":true} -->
		<div class="wp-block-cover has-background-dim has-parallax alignfull" style="background-image:url(https://via.placeholder.com/1600x900)"></div>
		<!-- /wp:cover -->
		<?php

		$out = ob_get_clean();
		return $out;
	}
}