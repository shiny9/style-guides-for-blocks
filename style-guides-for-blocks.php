<?php

/*
 * Plugin Name: Style Guides for Gutenberg Blocks
 * Description: Creates posts in draft mode showcasing all default Gutenberg blocks, for styling purposes.
 * Version:     1.0
 * Author:      Robert Gillmer
 * Author URI:  https://www.robertgillmer.com
 * Text Domain: SGB
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// Needed for sgb_add_settings_link() filter.
define( 'SGB_BASENAME', plugin_basename(__FILE__) );

include_once( plugin_dir_path( __FILE__ ) . 'autoload.php' );

include_once( plugin_dir_path( __FILE__ ) . 'general.php' );
include_once( plugin_dir_path( __FILE__ ) . 'admin-menu-functions.php' );
include_once( plugin_dir_path( __FILE__ ) . 'add-sgb-posts.php');