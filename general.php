<?php

function sgb_enqueue_scripts() {
	wp_register_script( 'sgb-ajax', plugin_dir_url( __FILE__ ) . '/js/sgb.js', array( 'jquery' ), false, true );
}

add_action( 'admin_enqueue_scripts', 'sgb_enqueue_scripts' );