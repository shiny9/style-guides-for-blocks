<?php

// Admin menu related functions

// @TODO: Class this up

add_action( 'admin_menu', 'sgb_add_admin_menu' );
add_action( 'admin_init', 'sgb_settings_init' );

function sgb_add_admin_menu() { 
	add_management_page( 'Style Guides for Gutenberg Blocks', 'Style Guides for Gutenberg Blocks', 'manage_options', 'style-guides-for-gutenberg-blocks', 'sgb_options_page' );
}

function sgb_settings_init() { 
	register_setting( 'sgb_plugin_page', 'sgb_post_types', 'sgb_admin_notices' );

	add_settings_section(
		'sgb_plugin_page_section', 
		'',
		'sgb_settings_section_callback', 
		'sgb_plugin_page'
	);

	add_settings_field( 
		'sgb_cpt_selection', 
		__( 'Which post types do you want to render these blocks on?', 'sgb' ), 
		'sgb_cpt_selection_render', 
		'sgb_plugin_page', 
		'sgb_plugin_page_section' 
	);
}

function sgb_admin_notices( $input ) {
	add_settings_error( 'sgb_post_types', 'sgb-update', 'Your posts have been generated!', 'updated' );

	return $input;
}

function sgb_cpt_selection_render() {
	$all_post_types = get_post_types(
		array(
			'public'	=> true,
		),
		'objects'
	);

	unset( $all_post_types[ 'attachment' ] );

	$options = get_option( 'sgb_post_types' );
	foreach( $all_post_types as $cpt ) {
		if( isset( $options[ $cpt->name ] ) ) {
			$checked = $options[ $cpt->name ];
		} else {
			$checked = '';
		}
		?>
		<input class="sgb-cpt-type" data-cpt="<?php echo $cpt->name; ?>" type='checkbox' name='sgb_post_types[<?php echo $cpt->name; ?>]' <?php checked( $checked, 1 ); ?> value='1'>
		<label for='sgb_post_types[<?php echo $cpt->name; ?>]'><?php echo $cpt->label; ?></label>
		<br />
		<?php
	}
}

function sgb_settings_section_callback() { 
	echo __( '<p>Style guides will be made for each of the selected custom post types.</p>', 'sgb' );
}

function sgb_options_page() {
	wp_enqueue_script( 'sgb-ajax' );

	$script_params = array(
		'sgbAddNonce'		=> wp_create_nonce( 'sgb-add-nonce' ),
		'sgbDeleteNonce'	=> wp_create_nonce( 'sgb-delete-nonce' ), // upcoming
	);
	
	wp_localize_script( 'sgb-ajax', 'sgbAjax', $script_params );

	?>
	<h2>Gutenberg Block Style Guides</h2>
	<?php settings_errors(); ?>
	<form action='options.php' method='post' name="sgb-form" id="sgb-form">
		<?php
		settings_fields( 'sgb_plugin_page' );
		do_settings_sections( 'sgb_plugin_page' );
		submit_button( 'Save and Generate Posts' );
		?>
	</form>
	<?php
}

function sgb_add_settings_link( $links ) {
	$settings_link = '<a href="' . esc_url( get_admin_url( null, 'tools.php?page=gutenberg-block-style-guides' ) )  . '">' . __( 'Settings' ) . '</a>';
	array_unshift( $links, $settings_link );
	return $links;
}

add_filter( 'plugin_action_links_' . SGB_BASENAME, 'sgb_add_settings_link' );