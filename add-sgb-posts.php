<?php

function sgb_add_posts() {

	check_ajax_referer( 'sgb-add-nonce', 'sgbAddNonce' );

	$post_types = isset( $_POST[ 'selectedTypes' ] ) ? ( array ) $_POST[ 'selectedTypes' ] : array();

	$post_types = array_map( 'sanitize_text_field', $post_types );

	$class_templates = array(
		'Style Guide for Audio/Video Blocks'             => '\SGB\CoreWP\Audio_Video',
		'Style Guide for Columns Blocks'                 => '\SGB\CoreWP\Columns',
		'Style Guide for Covers Blocks'                  => '\SGB\CoreWP\Covers',
		'Style Guide for Galleries Blocks'               => '\SGB\CoreWP\Galleries',
		'Style Guide for Images Blocks'                  => '\SGB\CoreWP\Images',
		'Style Guide for Other Layouts Blocks'           => '\SGB\CoreWP\Other_Layouts',
		'Style Guide for Other Typographical Blocks'     => '\SGB\CoreWP\Other_Typographical',
		'Style Guide for Paragraphs and Header Blocks'   => '\SGB\CoreWP\Paragraphs_Headers',
		'Style Guide for Quotes and Pull Quotes Blocks'  => '\SGB\CoreWP\Quotes_Pullquotes',
		'Style Guide for Read More Blocks'               => '\SGB\CoreWP\Read_More',
		'Style Guide for Standard Widgets Blocks'        => '\SGB\CoreWP\Standard_Widgets',
	);

	global $wpdb;

	foreach( $post_types as $post_type ) {
		foreach( $class_templates as $k => $v ) {

			$slug = sanitize_title( $k, '', 'save' );

			// Does the post exist? If so, skip it
			if( $wpdb->get_row( "SELECT post_title FROM wp_posts WHERE post_title = '" . $k . "' AND post_type = '" . $post_type . "'", 'ARRAY_A') ) {
				continue;
			}

			$markup_obj = new $v;
			$markup = $markup_obj->markup;

			$create_post = array(
				'post_title'   => $k,
				'post_type'    => $post_type,
				'post_content' => $markup,
				'post_status'  => 'draft',
				'meta_input'   => array(
					'created-by-sgb' => true,
				)
			);

			wp_insert_post( $create_post );
		}
	}

	// This is an AJAX call, so it dies at the end.

	wp_die();
}

// Gotsta gotta be logged in, so no `nopriv` hook needed.
add_action( 'wp_ajax_sgb_add_posts', 'sgb_add_posts' );